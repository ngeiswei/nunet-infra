# Increase log verbosity
log_level = "DEBUG"

data_dir = "/tmp/nomad_test_client_dir"

name = "nunet test server"

datacenter = "testing-nunet-io"

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

# Increase log verbosity
log_level = "DEBUG"

data_dir = "/tmp/nomad-client"

name = "dagim desktop"

datacenter = "dagim-home"

client {
    enabled = true
    servers = ["nomad.icog-labs.com:4647"]
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    gc {
      dangling_containers {
        enabled = false
      }
    }
  }
}

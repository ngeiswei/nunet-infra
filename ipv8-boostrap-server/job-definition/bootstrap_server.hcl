job "bootstrap-server" {
    datacenters = ["testing-nunet-io"]

    group "bootstrap-server" {
        count = 1
        task "bootstrap-server" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/nunet-infra/ipv8-server:latest"
                args = ["python3", "./bootstrap/tracker_plugin.py", "-p", " ${NOMAD_PORT_rpc}"]
            }
            resources {
                cpu    = 2000 # MHz
                memory = 1000 # MB
                network {
                    port "rpc" {}
                }
            }

        }
    }
}

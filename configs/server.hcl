log_level="DEBUG"

datacenter = "anton"
data_dir  = "/var/lib/nomad"

bind_addr = "0.0.0.0" # the default

advertise {
  # Defaults to the first private IP address.
   http = "nomad.icog-labs.com:4646"
   rpc  = "nomad.icog-labs.com:4647"
}

server {
  enabled = true
  bootstrap_expect = 1
}

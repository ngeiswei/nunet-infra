job "consul" {
    datacenters = ["kabir-desktop"]

    group "consul" {
        count = 1
        task "consul" {
            driver = "raw_exec"

            config {
                command = "consul"
                args = ["agent", "-data-dir=/var/lib/consul", "-client=0.0.0.0"]
            }

        }
    }
}


#!/bin/bash

nunet-adapter() {
    cd /home/$USER/ai-dsl/nunet-adapter
    git checkout 36-enable-local-deployment-of-services-for-experimenting-with-metadata-gathering-and-workflow   
	docker build -t nunet-adapter-ai-dsl /home/$USER/ai-dsl/nunet-adapter/
}

fake-news-score() {
    cd /home/$USER/ai-dsl/fake_news_score
    git checkout 19-enable-local-deployment-of-services-for-experimenting-with-metadata-gathering-and-workflow       
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
	docker build -t news-score-ai-dsl /home/$USER/ai-dsl/fake_news_score/
}

uclnlp() {
    cd /home/$USER/ai-dsl/uclnlp
    git checkout 15-enable-local-deployment-of-services-for-experimenting-with-metadata-gathering-and-workflow   
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
	docker build -t uclnlp-ai-dsl /home/$USER/ai-dsl/uclnlp/
}

athene() {
    cd /home/$USER/ai-dsl/athene
    git checkout 20-enable-local-deployment-of-services-for-experimenting-with-metadata-gathering-and-workflow    
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
    docker build -t athene-ai-dsl /home/$USER/ai-dsl/athene/athene_snet_grpc/

    docker build -t athene-system-ai-dsl /home/$USER/ai-dsl/athene/athene_system/

}


tokenomics() {
    cd /home/$USER/ai-dsl/tokenomics-api-eth
    git checkout 16-enable-local-deployment-of-services-for-experimenting-with-metadata-gathering-and-workflow
	docker build --build-arg CONFIG="TEST" --build-arg rpc_address=$rpc_address -t  tokenomics-ai-dsl  /home/$USER/ai-dsl/tokenomics-api-eth/
}

all() {
    nunet-adapter
    athene
    uclnlp
    fake-news-score
    tokenomics
}

help () {
    echo "Usage: bash build.sh OPTION [SERVICE]"
    echo "  Options:"
    echo "    nunet-adapter        build the nunet-adapter docker image"

    echo "    fake-news-score        build the fake-news-score docker image"    
    echo "    uclnlp        build the uclnlp docker image"
    echo "    athene        build the athene docker image"
    echo "    all        build all docker image"

}

case $1 in
    nunet-adapter) nunet-adapter ;;

    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    tokenomics) tokenomics;;
    all) all ;;
  *) help ;;
esac
